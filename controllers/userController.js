const User = require ("../model/Users");
const bcrpyt= require("bcrypt")
const auth= require("../auth")
const Course = require("../model/Course");


//check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result =>{

		if (result.length > 0) {
			return true
		}
		else{
			return false
		}
	})
}


// controller for user registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// bcrpyt.hashSync(<dataToBeHased, saltRound)
		password: bcrpyt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) =>{
		if (error){
			return false
		}
		else{
			return true
		}
	})
}


//user authentication
// check routes and controller for loginUser
module.exports.loginUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result => {

		if (result ==null) {
			return false
		} else {
			const isPasswordCorrect = bcrpyt.compareSync(reqBody.password,result.password);

			// bcrpy.compareSync(<password from request body / data to be compared>, <data from the datbase>)

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else {
				return false
			}
		}
	})
}


// activity 2. get profile
module.exports.getProfile = (reqBody) =>{
	return User.findOne({_id:reqBody.id}).then(result =>{
		

			result.password= ""
			return result
		
	})//.catch(error => {return "id does not exist"});
}


// enroll user to a class 
module.exports.enroll = async (data)=>{
	if (data.isAdmin){

		return false
	}
	else{
	let isUserUpdated = await User.findById(data.userId).then(user =>{

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user,error) => {

			if(error){
				return false
			}
			else{
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{
		course.enrollees.push({userId: data.userId})
		return course.save().then((course,error) =>{
			if(error){
				return false
			}
			else {
				return true
			}
		})
	})

	if (isUserUpdated && isCourseUpdated){
		return true
	}else{
		return false
	}
	}
}

// 