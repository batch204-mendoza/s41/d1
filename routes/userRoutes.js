const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");

// route for checking if the urser's email already exists in our database
router.post("/checkEmail", (req,res) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// routes for user registration
router.post("/register", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//User Authentication
router.post("/login", (req,res)=> {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// activity step 1 retrieve details of usercourseController.updateCourse(req.params, req.body, userData). then(resultFromController => res.send(resultFromController))courseController.updateCourse(req.params, req.body, userData). then(resultFromController => res.send(resultFromController))
router.post("/details", auth.verify, (req,res)=> {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	console.log(req.headers.authorization);
	userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
});


module.exports = router;





/*

const cutPieces = function (fruit) {
	return fruit * 4;
};

const fruitProcessor = function (apples, oranges) {
	
	const applePieces = cutPieces(apples);
	const orangePieces = cutPieces(oranges);

	const juice = `Juice with ${applePieces} pieces of apple and ${orangePieces} pieces of orange.`;
	return juice;
};

console.log(fruitProcessor(2, 3));




*/


// route to enroll user to a course


router.post("/enroll", auth.verify, (req, res) => {


	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

});